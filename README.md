# jobrunner

### How to generate jar file

Current version: `0.1`

To generate .jar file run the following command:

* `sbt package`

Generated jar will be located under:

* `../target/scala-2.12/jobrunner_2.12-{current-version}.jar`

### Implementation

Implementing class is:

* `com.workday.techtest.ParStreamJobRunner`

### Assumptions

Implementation was based on the following assumptions:

* all jobs that are popped from the JobQueue have to be executed
* there is no execution priority as long as all popped jobs are executed
* job execution can fail (throwing an Exception)
* only job executed without throwing an Exception is considered successful
* it is not allowed to pop more jobs from the JobQueue than it is necessary to satisfy the jobCount
* jobs have to be executed in parallel to provide a good performance
* the order of execution of the jobs that were popped is not important
* jobs have to be popped lazily in order to save memory
