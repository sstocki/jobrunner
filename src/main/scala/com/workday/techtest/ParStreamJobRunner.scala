package com.workday.techtest

import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

class ParStreamJobRunner extends JobRunner with LazyLogging {

  override def runner(jobQueue: JobQueue, jobCount: Long): Unit = {

    def streamOfJobs: Stream[Job] = jobQueue.pop() #:: streamOfJobs

    val nProcessors = Runtime.getRuntime.availableProcessors()

    def nOfSuccess(t: Int): Int =
      streamOfJobs
        .take(t)
        .par
        .aggregate(0)(
          (acc, job) => {
            Try(job.execute()) match {
              case Success(_) =>
                logger.debug(s"job executed successfully: $job")
                acc + 1
              case Failure(f) =>
                logger.debug(f.getMessage)
                acc
            }
          },
          _ + _
        )

    @tailrec
    def iter(numberOfJobsLeft: Long): Unit = {
      numberOfJobsLeft match {
        case 0                            =>
        case n if n >= nProcessors.toLong => iter(numberOfJobsLeft - nOfSuccess(nProcessors))
        case m                            => iter(numberOfJobsLeft - nOfSuccess(m.toInt))
      }
    }

    logger.info(s"ParStreamJobRunner.runner started running using $nProcessors processors")
    iter(jobCount)
    logger.info(s"ParStreamJobRunner.runner finished processing $jobCount jobs")
  }

  override val version: String = "536543A4-4077-4672-B501-3520A49549E6"
}
