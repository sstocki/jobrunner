package com.workday.techtest

import org.slf4j.LoggerFactory

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

class SimpleJobRunner extends JobRunner {

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def runner(jobQueue: JobQueue, jobCount: Long): Unit = {

    @tailrec
    def iter(numberOfJobsLeft: Long): Unit = numberOfJobsLeft match {
      case 0 =>
      case _ =>
        val job = jobQueue.pop()
        Try(job.execute()) match {
          case Success(_) =>
            logger.debug(s"job executed successfully: $job")
            iter(numberOfJobsLeft - 1)
          case Failure(f) =>
            logger.debug(f.getMessage)
            iter(numberOfJobsLeft)
        }
    }

    logger.info("SimpleJobRunner.runner started")
    iter(jobCount)
    logger.info(s"SimpleJobRunner.runner finished processing $jobCount jobs")
  }

  override val version: String = "536543A4-4077-4672-B501-3520A49549E6"
}
