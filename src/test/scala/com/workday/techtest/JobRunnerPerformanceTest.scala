package com.workday.techtest

import com.typesafe.scalalogging.LazyLogging
import org.scalameter.{Quantity, measure}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class JobRunnerPerformanceTest extends AnyWordSpec with Matchers with LazyLogging {

  private val jobCount: Long = 100L

  "SimpleJobRunner" should {
    val runner: JobRunner = new SimpleJobRunner

    "run jobs in sequence" in {
      val jobQueue: JobQueue     = new TestJobQueue(flaky = true)
      val time: Quantity[Double] = measure { runner.runner(jobQueue, jobCount) }
      logger.info(s"SimpleJobRunner execution time = $time")
    }
  }

  "ParStreamJobRunner" should {
    val runner: JobRunner = new ParStreamJobRunner

    "run jobs in parallel" in {
      val jobQueue: JobQueue     = new TestJobQueue(flaky = true)
      val time: Quantity[Double] = measure { runner.runner(jobQueue, jobCount) }
      logger.info(s"ParStreamJobRunner execution time = $time")
    }
  }

  "SimpleJobRunner vs ParStreamJobRunner" should {
    val jobCountCompare: Long = 500L

    "compare performance between two implementations" in {
      val simRunner: JobRunner = new SimpleJobRunner
      val parRunner: JobRunner = new ParStreamJobRunner

      val jobQueue: JobQueue        = new TestJobQueue(flaky = false)
      val timeSim: Quantity[Double] = measure { simRunner.runner(jobQueue, jobCountCompare) }
      val timePar: Quantity[Double] = measure { parRunner.runner(jobQueue, jobCountCompare) }

      logger.info(s"SimpleJobRunner execution time = $timeSim")
      logger.info(s"ParStreamJobRunner execution time = $timePar")
    }
  }
}
