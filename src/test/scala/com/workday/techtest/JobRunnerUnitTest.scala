package com.workday.techtest

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class JobRunnerUnitTest extends AnyWordSpec with Matchers with LazyLogging {

  private val jobCount: Long = 200L

  "SimpleJobRunner" should {
    val runner: JobRunner = new SimpleJobRunner

    "run jobs in sequence" in {
      val jobQueue: TestJobQueue = new TestJobQueue(flaky = false)
      runner.runner(jobQueue, jobCount)
      jobQueue.getCurrentIndex shouldBe jobCount
    }

    "return version '536543A4-4077-4672-B501-3520A49549E6'" in {
      runner.version shouldBe "536543A4-4077-4672-B501-3520A49549E6"
    }
  }

  "ParStreamJobRunner" should {
    val runner: JobRunner = new ParStreamJobRunner

    "run jobs in parallel" in {
      val jobQueue: TestJobQueue = new TestJobQueue(flaky = false)
      runner.runner(jobQueue, jobCount)
      jobQueue.getCurrentIndex shouldBe jobCount
    }

    "return version '536543A4-4077-4672-B501-3520A49549E6'" in {
      runner.version shouldBe "536543A4-4077-4672-B501-3520A49549E6"
    }
  }
}
