package com.workday.techtest

import java.util.concurrent.atomic.AtomicLong

import scala.util.Random

class TestJobQueue(flaky: Boolean) extends JobQueue {

  private val currentIndex: AtomicLong = new AtomicLong(0L)
  private val rand: Random             = new Random()

  override def pop(): Job = {
    val index: Long      = currentIndex.getAndIncrement()
    val customerId: Long = index % 12
    val duration: Int    = if (index % 3 == 0) 30 else 80
    val fail: Boolean    = if (flaky) rand.nextBoolean() else false
    if (index % 20 == 0) {
      Thread.sleep(300)
    } else {
      Thread.sleep(100)
    }
    TestJob(customerId, index, duration, fail)
  }

  def getCurrentIndex: Long = currentIndex.get()
}
