package com.workday.techtest

case class TestJob(customerId: Long, uniqueId: Long, duration: Int, fail: Boolean) extends Job {

  override def execute(): Unit = {
    Thread.sleep(duration)
    if (fail) throw new RuntimeException(s"timeout: $this")
  }
}
