name := "jobrunner"

version := "0.1"

scalaVersion := "2.12.11"

libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-simple" % "1.7.30",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.scalatest" %% "scalatest" % "3.1.2" % Test,
  "com.storm-enroute" %% "scalameter-core" % "0.19" % Test
  //  "ch.qos.logback" % "logback-classic" % "1.2.3",
)